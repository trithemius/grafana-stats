package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"net/http"
	"net/url"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

// Struct to represent the JSON data
type PluginData struct {
	Name      string `json:"name"`
	Downloads int    `json:"downloads"`
}

var (
	downloadsGauge = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "plugin_downloads",
			Help: "Number of downloads for a plugin",
		},
		[]string{"name"},
	)

	registry = prometheus.NewRegistry()
)

func main() {
	port := flag.Int("p", 9000, "Port for the Prometheus metrics server")
	interval := flag.Int("t", 30, "Interval for fetching data in minutes")
	proxy := flag.String("proxy", "", "HTTP proxy URL (e.g., http://proxy.example.com:8080)")
	flag.Parse()

	registry.MustRegister(downloadsGauge)

	go fetchAndExposeMetrics(time.Duration(*interval), *proxy)
	http.Handle("/metrics", promhttp.HandlerFor(registry, promhttp.HandlerOpts{}))
	http.ListenAndServe(fmt.Sprintf(":%d", *port), nil)
}

func fetchAndExposeMetrics(interval time.Duration, proxy string) {
	client := &http.Client{}
	if proxy != "" {
		proxyURL, err := url.Parse(proxy)
		if err == nil {
			client.Transport = &http.Transport{
				Proxy: http.ProxyURL(proxyURL),
			}
		}
	}

	for {
		// Fetch data from both URLs
		dataMetrics, errMetrics := fetchPluginData("https://grafana.com/api/plugins/oci-metrics-datasource", client)
		dataLogs, errLogs := fetchPluginData("https://grafana.com/api/plugins/oci-logs-datasource", client)

		if errMetrics != nil {
			fmt.Println("Error fetching data from oci-metrics-datasource:", errMetrics)
		} else {
			exportMetrics(dataMetrics)
		}

		if errLogs != nil {
			fmt.Println("Error fetching data from oci-logs-datasource:", errLogs)
		} else {
			exportMetrics(dataLogs)
		}

		// Wait for the specified interval before fetching data again
		time.Sleep(time.Minute * interval)
	}
}

func fetchPluginData(url string, client *http.Client) (*PluginData, error) {
	response, err := client.Get(url)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var pluginData PluginData
	err = json.NewDecoder(response.Body).Decode(&pluginData)
	if err != nil {
		return nil, err
	}

	return &pluginData, nil
}

func exportMetrics(data *PluginData) {
	labels := prometheus.Labels{"name": data.Name}
	downloadsGauge.With(labels).Set(float64(data.Downloads))
}

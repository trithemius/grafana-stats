# grafana_stats - Prometheus Grafana Statistics for Grafana OCI Logs amd Metrics plugins

`grafana_stats` is a Prometheus metric collector, written in Go, that collects downloads counters from the two OCI Cloud Grafana plugins. By default, it exposes metrics on port 9000, which can be accessed using `curl` or integrated into your collector system.

## Installation

You can download the ZIP file containing all `grafana_stats` binaries from the latest GitLab job artifacts for the project:

- [Download artifacts.zip (latest)](https://gitlab.com/trithemius/grafana-stats)

Once you've downloaded the ZIP file, extract it to your desired location. You will find the binaries for various operating systems and architectures inside the extracted folder.

## Usage

The script supports the following command-line flags:

- `-p` or `--port`: Specify the port for the Prometheus metrics server (default: 9000).

- `-t` or `--time`: Specify the interval for fetching data in minutes (default: 30 minutes).

Example usage:


### Windows (AMD64)

```bash
./grafana_stats_windows_amd64.exe
```

### Linux (AMD64)

```bash
./grafana_stats_linux_amd64
```

### Linux (ARM64)

```bash
./grafana_stats_linux_arm64
```

### macOS (AMD64)

```bash
./grafana_stats_macos_amd64
```

### macOS (ARM64)

```bash
./grafana_stats_macos_arm64
```

You can access the metrics using curl as follows:
```bash
curl http://localhost:9000/metrics
```

## Metrics

The script exposes the following Prometheus metric:

- `plugin_downloads`: The number of downloads for a plugin, labeled by the plugin's name.

## Data Sources

The script fetches data from the following JSON endpoints:

1. [https://grafana.com/api/plugins/oci-metrics-datasource](https://grafana.com/api/plugins/oci-metrics-datasource)

2. [https://grafana.com/api/plugins/oci-logs-datasource](https://grafana.com/api/plugins/oci-logs-datasource)

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.

